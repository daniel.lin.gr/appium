#!/usr/bin/python
# -*- coding: utf-8 -*-

#=== KKBOX Unit Test ===
# Date    : 2018.10.12
# Program : login
# Author  : Daniel
#===========================

from time import sleep
from appium import webdriver
import unittest

#==============================
# function :login_function
# Parameter :	
#	driver->must given
def login_function(driver,wait_time=5,user_id="ac001",password="ac001"):
	ex=""
	try:
		#1/0  # for debug Error Case
		# Get Web Driver
		sleep(2)		
	
		# Login	
		if(1):
			# 點擊'登入'
			element = driver.find_element_by_id('com.skysoft.kkbox.android:id/button_login')
			#element.send_keys(id)
			# 點擊一次，為了讓鍵盤統一出現，SOP 流程
			element.click()
			sleep(1)
			
			#選擇登入方式
			if(1):
				# email
				element = driver.find_element_by_id('com.skysoft.kkbox.android:id/button_login_with_email')
				element.click()
				sleep(1)
			
			#輸入帳密
			if(1):
				#帳號
				id = 'qa.habook@gmai.com'
				element = driver.find_element_by_id('com.skysoft.kkbox.android:id/text_uid')
				element.click()
				# 隱藏鍵盤
				sleep(1)
				driver.hide_keyboard()
				element.send_keys(id)
				#密碼
				pwd = '80650390'
				element = driver.find_element_by_id('com.skysoft.kkbox.android:id/text_password')
				element.click()
				# 隱藏鍵盤
				sleep(1)
				driver.hide_keyboard()				
				element.send_keys(pwd)
				#登入		
				element = driver.find_element_by_id('com.skysoft.kkbox.android:id/button_login')
				element.click()
			
			if(0):
				# 隱藏鍵盤
				driver.hide_keyboard()
				# 輸入密碼
				pwd = password
				#pwd="qa001"
				element = driver.find_element_by_id('com.habook.aclassonemobile:id/login_pwd_edit')
				element.send_keys(pwd)
				element.click()
				# 隱藏鍵盤
				sleep(1)
				driver.hide_keyboard()			
				# 登入
				element = driver.find_element_by_id('com.habook.aclassonemobile:id/login')
				element.click()
				# 登入等待時間
				sleep(7)
			
			ret="OK"
	except Exception,ex:
		#print(Exception,":",ex)	
		ret="Fail"

	return ret,ex

	
#==============================
# function :get_driver_info
# Parameter : deviceName
#	retunr desired_caps[]
def get_driver_info(deviceName="LC57GYF00340"):
	ex=""
	
	try:	
		""" Default Device """
		""" HTC 820 """
		# 初始化資料
		desired_caps = {}
		# platformName
		desired_caps['platformName'] = 'Android'
		# platformVersion 系統版本
		desired_caps['platformVersion'] = '5.0.2'
		# deviceName 裝置名稱
		#desired_caps['deviceName'] = 'Android Emulator'
		desired_caps['deviceName'] = 'LC57GYF00340'
		# appPackage
		#desired_caps['appPackage'] = 'com.habook.aclassonemobile'
		desired_caps['appPackage'] = 'com.skysoft.kkbox.android'		
		#appActivity
		#desired_caps['appActivity'] = 'SplashActivity'
		desired_caps['appActivity'] = 'HomeActivity'
		
		""" Other Device """
		
		""" 夜神模擬器 """		
		if(deviceName== u"夜神模擬器"):
			# 初始化資料	
			desired_caps = {}
			# platformName
			desired_caps['platformName'] = 'Android'
			# platformVersion 系統版本
			desired_caps['platformVersion'] = '5.1.1'
			# deviceName 裝置名稱
			desired_caps['deviceName'] = 'Android Emulator'
			# appPackage
			desired_caps['appPackage'] = 'com.habook.aclassonemobile'
			#appActivity
			desired_caps['appActivity'] = 'SplashActivity'		
			driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
			#print u"夜神模擬器"
		
		""" IPHONE """
		if(deviceName=="IPHONE"):
			print "IPHONE"
		
		""" LAB Device """
		""" IPHONE """
		if(deviceName=="D70GLMB431203929"):
			#print "LAB PHONE"
			# 初始化資料
			desired_caps = {}
			# platformName
			desired_caps['platformName'] = 'Android'
			# platformVersion 系統版本
			desired_caps['platformVersion'] = '4.4.2'
			# deviceName 裝置名稱
			#desired_caps['deviceName'] = 'Android Emulator'
			desired_caps['deviceName'] = 'D70GLMB431203929'
			# appPackage
			desired_caps['appPackage'] = 'com.habook.aclassonemobile'
			#appActivity
			desired_caps['appActivity'] = 'SplashActivity'			
			
		ret="OK"
	except Exception,ex:
		print(Exception,":",ex)	
		
	return desired_caps

if __name__== "__main__":
	print ("This is main function called")
	
	#if(1):
	#	# 初始化資料	
	#	desired_caps = {}
	#	# platformName
	#	desired_caps['platformName'] = 'Android'
	#	# platformVersion 系統版本
	#	desired_caps['platformVersion'] = '5.0.2'
	#	# deviceName 裝置名稱
	#	#desired_caps['deviceName'] = 'Android Emulator'
	#	desired_caps['deviceName'] = 'LC57GYF00340'
	#	# appPackage
	#	desired_caps['appPackage'] = 'com.habook.aclassonemobile'
	#	#appActivity
	#	desired_caps['appActivity'] = 'SplashActivity'		
	#	driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
	
	if(1):
		# WebDriver
		# 取得裝置資訊
		desired_caps = {}
		desired_caps = get_driver_info()
		#desired_caps = get_driver_info(u"夜神模擬器")
		#desired_caps = get_driver_info("D70GLMB431203929")
	
		driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
		
		id ="qa001"
		pwd ="qa001"
		ret=login_function(driver=driver, user_id=id, password=pwd)	
		print("login_function : " + ret[0]+ "  " + str(ret[1]) )
		sleep(5)
		